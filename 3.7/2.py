'''メジアン
'''
def main():
    num = input()
    num = list(map(int,num.split(',')))
    if len(num) % 2 == 0:
        half = len(num)//2
        num = num[half-1:half+1]
        median = sum(num)/2
    else:
        median = num[len(num)//2]
    return median

if __name__ == '__main__':
    print(main())
