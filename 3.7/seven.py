'''整数の配列からモード(最頻値)をもとめる
'''

import random

def main():
    num = [random.randrange(999) for i in range(999)]
    counter = [0] * (max(num)+1)
    for n in num:
        counter[n] += 1 #counter
    return counter.index(max(counter))

if __name__ == '__main__':
    print(main())
