'''換字式の暗号作成
'''

import random

def main(plane):
    plane = [s for s in plane]
    planekey = plane[:]
    random.shuffle(planekey)
    cipher = dict(zip(plane, planekey))
    return(cipher)

if __name__ == '__main__':
    import string
    print(main(string.ascii_letters))
