'''配列と要素数を渡してソートされているかの bool を返す
要素数は len つかえばいいか
'''

def main(ls):
    maxm = ls[0]
    for i in range(len(ls)):
        if maxm > ls[i]:
            return False
        else:
            maxm = ls[i]
    else:
        return True

if __name__ == '__main__':
    main()
