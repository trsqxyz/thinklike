'''四分位数をもとめる
0.25, 0.50, 0.75
'''

students = (
    (87, 10001, "Fred"),
    (28, 10002, "Tom"),
    (100, 10003, "Alistair"),
    (78, 10004, "Sasha"),
    (84, 10005, "Erin"),
    (98, 10006, "Belinda"),
    (75, 10007, "Leslie"),
    (79, 10008, "Candy"),
    (81, 10009, "Aretha"),
    (68, 10010, "Veronica"),
)
quartile = {
    "Q1": {
        "position":0,
        "score": 0
    },
    "Q2": {
        "position": 0,
        "score": 0
    },
    "Q3": {
        "position": 0,
        "score": 0
    }
}
scores = sorted(tuple(student[0] for student in students))

def main():
    quartile["Q1"]["position"] = (1 + len(scores)) / 2
    quartile["Q2"]["position"] = (1 + quartile["Q1"]["position"]) / 2
    quartile["Q3"]["position"] = (quartile["Q1"]["position"] + len(scores)) / 2
    for k in quartile.keys():
        if quartile[k]["position"] - int(quartile[k]["position"]) > 0:
            position = int(quartile[k]["position"])
            quartile[k]["score"] = (scores[position] + scores[position+1]) / 2
        else:
            quartile[k]["score"] = scores[quartile[k]["score"]]
    return quartile

if __name__ == '__main__':
    print(main())
