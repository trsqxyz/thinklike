'''ランダム文字列から換字式の暗号を生成をするが
同じ文字を入れ替えないようにする
'''

import random
import string
import four

def main():
    plane = string.ascii_letters
    cipher = four.main(plane)
    checker = lambda x,y: x != y
    while not all(map(checker, cipher.keys(), cipher.values())):
        cipher = four.main(plane)
    return cipher
if __name__ == '__main__':
    print(main())
