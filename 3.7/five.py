'''換字式暗号の復号
'''
import four
from string import ascii_letters

def main():
    cipher = four.main(ascii_letters)
    plane = {v:k for k,v in cipher.items()}
    return cipher, plane

if __name__ == '__main__':
    print(main())
