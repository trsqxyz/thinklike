import sys
from itertools import product

class hangedman(object):
    """docstring for ClassName"""
    def __init__(self, word, length):
        with open("wordsEn.txt") as f:
            we = f.read().split("\n")
        pickups = list(filter(lambda x:word in x, we))
        self.words = list(filter(lambda x:len(x) == length, pickups))
        self.classified = {str(i):[] for i in range(length)}
        self.recoad = [word]
        self.length = length

    def classify(self):
        """文字の位置ごとに分類して最多の項目だけ残す"""
        self.classified = {str(i):[] for i in range(self.length)}
        for w in self.words[:]:
            try:
                self.classified[str(w.index(self.recoad[-1]))].append(w)
            except (ValueError, KeyError):
                pass
        else:
            return self.words_trim()

    def words_trim(self):
        """最多の要素以外を消す"""
        d = self.classified.copy()
        for k, v in d.items():
            if v != max(self.classified.values()):
                self.classified.pop(k)
        return list(self.classified.values())[0]

    def answer(self):
        """recoader の最新追加文字で words をフィルタリングして
        words を更新する
        """
        words = []
        for word in self.words[:]:
            for i in range(self.length):
                if self.recoad[-1] == word[i]:
                    words.append(word)
                    break
            else:
                self.words.remove(word)
        else:
            self.words = words

    def recoader(self, arg):
        """選んだ文字を保存"""
        self.recoad.append(arg)

    def judge(self):
        if len(self.words) == 1:
            print("User Win! {0}".format(self.words[0]))
            return True
        elif self.length <= len(self.recoad):
            print("HANGED. {0}".format(self.words))
            return True
        else:
            self.recoader(input("Choice word: "))
            self.answer()
            self.words = self.classify()
            return False

    def start(self):
        while not self.judge(): pass

    def restart(self):
        self.__init__(self.recoad[0], self.length)
        self.start()

def main(word, length):
    hm = hangedman(word, length)
    hm.start()

if __name__ == '__main__':
    main(sys.args[0], sys.args[1])
