def main(ls):
    print(ls[0])
    if len(ls) <= 1: return ls
    main(ls[1:])

def main2(ls):
    print(ls[-1])
    if len(ls) <= 1: return ls
    main2(ls[0:-1])
