def sum_plus_int(ls):
    if len(ls) == 0: return 0
    count = 0
    count += sum_plus_int(ls[1:])
    if ls[0] >= 1: count += 1
    return count

def is_odd_parity(ls):
    if len(ls) == 0: return False
    count = 0
    count += is_odd_parity(ls[1:])
    if ls[0] == 1: count += 1
    return True if count % 2 == 1 else False

def target_counter(arg, target):
    if len(arg) == 0: return 0
    count = 0
    count += target_here(arg[1:], target)
    if arg[0] == target: count += 1
    return count
