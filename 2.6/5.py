'''ルーン式によるチェックディジットの実装
ルーンチェックサムの検証
'''

def main():
    dcs = []
    digits = input()
    while True:
        dcs.append(digits)
        try:
            digits = input()
        except(EOFError):
            dcs.append(0)
            dcs = list(map(int, dcs))
            break
    print(sumcheck(sum_alldigits(oddDoubled(dcs))))

def oddDoubled(digits):
    for i,n in enumerate(digits):
        if i % 2 == 1:
            n = n*2
        digits[i] = n
    else:
        return digits

def sum_alldigits(digits):
    total = 0
    for i,n in enumerate(digits[:-1]):
        if i % 2 == 1:
            x = n // 10
            y = n - x*10
            total += x + y
        else:
            total += n
    else:
        return total

def sumcheck(digit):
    balance = digit % 10
    if balance > 0:
        digit += 10 - balance
    return int(digit)

if __name__ == '__main__':
    main()
