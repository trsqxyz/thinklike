'''ハッシュ記号・スペース・行末文字のいずれかの文字を出力する文だけをつかって
以下の図形をつくるプログラムを書こう。
1. 逆三角形
'''

def main():
    for i in range(5):
        line = " "*i
        line += "#"*(8-i*2)
        print(line)

if __name__ == '__main__':
    main()
