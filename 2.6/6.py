'''10進数と2進数の変換
判定がよくわからない
関数あるけど自分でやってみる
'''

def main():
    num = int(input('number: '))
    func = input('b2d or d2b: ')
    print(b2d(num) if func == 'b2d' else d2b(num))

def d2b(num, binary=None):
    if binary == None: binary = []
    binary.append(num % 2)
    num = num // 2
    if num > 0:
        binary = d2b(num, binary)
    else:
        binary.reverse()
        binary = ''.join(list(map(str,binary)))
    return binary

def b2d(num):
    numls = list(map(int, str(num)))
    num = 0
    for i,n in enumerate(reversed(numls)):
        num += n*2**i
    return num

if __name__ == '__main__':
    main()
