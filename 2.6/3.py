'''ハッシュ記号・スペース・行末文字のいずれかの文字を出力する文だけをつかって
以下の図形をつくるプログラムを書こう。
3. ひとでみたいなの
'''

def main():
    lines = []
    for i in range(4):
        line = " "*i + "#"*(i+1) + " "*(6-i*2)
        line += " "*(6-i*2) + "#"*(i+1)
        print(line)
        lines.append(line)
    else:
        for l in reversed(lines):
            print(l)

if __name__ == '__main__':
    main()
