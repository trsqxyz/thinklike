'''自分で設計してみよう: ハッシュ記号を使った対称的なパターンを考えて、
これまでと同じルールでその図形をつくるプログラムを書いてみよう
'''

def main():
    for i in range(2):
        lines = []
        for i in range(4):
            line = " "*(3-i) + "#"*(i+3)
            line += line[::-1]
            print(line)
            lines.append(line)
        else:
            for l in reversed(lines):
                print(l)

if __name__ == '__main__':
    main()
