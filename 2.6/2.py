'''ハッシュ記号・スペース・行末文字のいずれかの文字を出力する文だけをつかって
以下の図形をつくるプログラムを書こう。
2. ひし形
'''

def main():
    lines = []
    for i in range(4):
        line = " "*i
        line += "#"*(8-i*2)
        print(line)
        lines.append(line)
    else:
        for l in reversed(lines):
            print(l)

if __name__ == '__main__':
    main()
