"""-1 の element だけソートしないようにする
"""

def main(arg):
    posi = [i for i in arg if i > 0]
    posi.sort()
    count = 0
    for i,a in enumerate(arg[:]):
        if a > 0:
            arg[i] = posi[count]
            count += 1
    return arg
