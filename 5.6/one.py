class Cars(object):
    """Car date class
    """
    def __init__(self, car=None, maker=None, model=None):
        self.car = car
        self.maker = maker
        self.model = model

    def car(self):
        return self.car
    def maker(self):
        return self.maker
    def model(self):
        return self.model

    def new_car(self, car):
        self.car = car
    def new_maker(self, maker):
        self.maker = maker
    def new_model(self, model):
        self.model = model
