import one
class CarsInfomation(one.Cars):
    """docstring for ClassName"""
    def __init__(self, car=None, maker=None, model=None):
        super(CarsInfomation, self).__init__(car, maker, model)
        self.infomation = "{0}, {1}, {2}.".format(self.model, self.maker, self.car)

    def infomation(self):
        return self.infomation
